export default {
    host: process.env.MONGOEXPRESS_HOST,
    port: process.env.MONGOEXPRESS_PORT,
    user: process.env.MONGOEXPRESS_USER,
    password: process.env.MONGOEXPRESS_PASSWORD,
    admin_user: process.env.MONGOEXPRESS_ADMIN_USER,
    admin_password: process.env.MONGOEXPRESS_ADMIN_PASSWORD
  }