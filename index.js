const express = require('express');
const routes = require('./router/route');
const cors = require('cors');
const path = require('path');
const http = require('http');
const helmet = require('helmet');
const bodyParser = require("body-parser");
const consul_catalog = require('./consul/service-discovery');

const app = express();
const server = http.Server(app);

const PORT = 8780;
const HOST = '0.0.0.0';

app.use(bodyParser.urlencoded({extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(express.json());
app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads')));
app.use(helmet());
app.use(routes);
consul_catalog;

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
//server.listen(3131);