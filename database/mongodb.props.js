const mongoose = require('mongoose');

mongoose.connect('mongodb://', { useUnifiedTopology: true });

mongoose.Promise = global.Promise;
console.log(mongoose.connection.readyState);

module.exports = mongoose;
