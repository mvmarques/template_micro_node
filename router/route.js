const express = require('express');

const authMiddleware = require('../src/middleware/auth.middleware')

const HealthCheckController = require('../src/controllers/HealthCheck.controller');

const routes = express.Router();

routes.use(authMiddleware.validateUser);
routes.get('/health-check', HealthCheckController.index);


module.exports = routes;