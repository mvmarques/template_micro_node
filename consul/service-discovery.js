const consul = require('consul')();
const os = require('os');
const uuid = require('uuid');


const PID = process.pid;
const PORT = Math.floor(process.argv[2]);
const HOST = os.hostname();
const CONSUL_ID = `data-${HOST}-${PORT}-${uuid.v4()}`;
console.log(PORT)
console.log(process.argv[2])

process.stdin.resume();

let details = {
    name: 'data',
    address: HOST,
    check: {
      ttl: '10s',
      deregister_critical_service_after: '1m'
    },
    port: PORT,
    id: CONSUL_ID
}

console.log(os.hostname())
console.log(`PID: ${PID}, PORT: ${PORT}, ID: ${CONSUL_ID}`);


let agent = consul.agent.service.register(details, err => {
    if (err) {
      throw new Error(err);
    }
    console.log('Serviço registrado no consul');

    setInterval(() => {
      consul.agent.check.pass({id:`service:${CONSUL_ID}`}, err => {
        if (err) throw new Error(err);
        console.log('Serviço ok - health check ');
      });
    }, 5 * 1000);

    process.on('SIGINT', () => {
      console.log('SIGINT. De-Registering...');
      let details = {id: CONSUL_ID};
      consul.agent.service.deregister(details, (err) => {
        console.log('de-registered.', details.id);
        process.exit();
      });
    });
  });

module.exports = agent